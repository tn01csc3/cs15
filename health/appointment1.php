<!DOCTYPE html>
<html lang="en">
<head>
<title>Appointment</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Health medical template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
</head>
<body>

<div class="super_container">

    <!-- Menu -->

    <div class="menu trans_500">
        <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
            <div class="menu_close_container"><div class="menu_close"></div></div>
            <form action="#" class="menu_search_form">
                <input type="text" class="menu_search_input" placeholder="Search" required="required">
                <button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
            <ul>
                <li class="menu_item"><a href="index.html">Home</a></li>
                <li class="menu_item"><a href="#">About us</a></li>
                <li class="menu_item"><a href="#">Services</a></li>
                <li class="menu_item"><a href="news.html">News</a></li>
                <li class="menu_item"><a href="contact.html">Contact</a></li>
            </ul>
        </div>
        <div class="menu_social">
            <ul>
                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
    
    <!-- Home -->

    <div class="home">
        <!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8"></div>

        <!-- Header -->

        <header class="header" id="header">
            <div>
                <div class="header_top">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="header_top_content d-flex flex-row align-items-center justify-content-start">
                                    <div class="logo">
                                        <a href="#">health<span>+</span></a>    
                                    </div>
                                    <div class="header_top_extra d-flex flex-row align-items-center justify-content-start ml-auto">
                                        <div class="header_top_nav">
                                            <ul class="d-flex flex-row align-items-center justify-content-start">
                                                <li><a href="#">Help Desk</a></li>
                                                <li><a href="#">Emergency Services</a></li>
                                                <li><a href="appointment1.php">Appointment</a></li>
                                            </ul>
                                        </div>
                                        <div class="header_top_phone">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>+34 586 778 8892</span>
                                        </div>
                                    </div>
                                    <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header_nav" id="header_nav_pin">
                    <div class="header_nav_inner">
                        <div class="header_nav_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
                                            <nav class="main_nav">
                                                <ul class="d-flex flex-row align-items-center justify-content-start">
                                                    <li><a href="index.html">Home</a></li>
                                                    <li class="active"><a href="about.html">About Us</a></li>
                                                    <li><a href="services.html">Services</a></li>
                                                    <li><a href="news.html">News</a></li>
                                                    <li><a href="contact.html">Contact</a></li>
                                                </ul>
                                            </nav>
                                            <div class="search_content d-flex flex-row align-items-center justify-content-end ml-auto">
                                                <form action="#" id="search_container_form" class="search_container_form">
                                                    <input type="text" class="search_container_input" placeholder="Search" required="required">
                                                    <button class="search_container_button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </header>

        <div class="home_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <!--<div class="home_title">About us</div>-->
                            <h2 class="d-flex flex-row align-items-center justify-content-start" >HELLO!(patient)</h2>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- appointment-->



            <div class="container">
                
                
                    <ul class="d-flex flex-row align-items-center justify-content-start">
                                <li><div class="button about_button ml-auto mr-auto"><a href="appointment1.php"><span>Appointment</span><span>Appoinment</span></a></div></li>
                                <li><div class="button about_button ml-auto mr-auto"><a href="history.php"><span>History</span><span>History</span></a></div></li>
                                <li><div class="button about_button ml-auto mr-auto"><a href="notification.php"><span>Notification</span><span>Reminder/Notification</span></a></div></li>
                            </ul>

       
        <form action="appointment1.php" method="post">
        <h3 style="color:black;">To take an appointment please fill the following details:</h3>
        <div class="part1">
            
        <h3 style="color:black;"><input type="radio" name="search" value="symptoms" id="radio1" onClick="show_hide('symptoms', 'drname')" checked />Enter symptoms or problems:</h3>
        <div id="symptoms" style="display: block;"><br><br>
             <span class="custom-dropdown">
            <select id="problems" name="problems">Select
            <option value="Asthma">Asthma</option>
            <option value="Eczema">Eczema</option>
            <option value="Food allergies">Food allergies</option>
            <option value="Insect sting allergies">Insect sting allergies</option>
            
            <option value="High blood pressure">High blood pressure</option>
            <option value="Heart attack">Heart attack</option>
            <option value="Heart failure">Heart failure</option>

            <option value="Calcium and bone disorders">Calcium and bone disorders</option>
            <option value="Infertility">Infertility</option>
            <option value="Thyroid problems">Thyroid problem</option>
            <option value="Diabetes">Diabetes</option>

            <option value="Abdominal pain">Abdominal pain</option>
             <option value="Ulcers">Ulcers</option>
              <option value="Diarrhea">Diarrhea</option>
               <option value="Jaundice">Jaundice</option>
                <option value="Cancers in your digestive organs">Cancers in your digestive organs</option>
           </select>
       </span>

        </div>
            

            <br /><h2 style="text-align: center; border:solid;;">OR</h2><br></u>
            

            
        <h3 style="color:black;"><input type="radio" name="search" value="drname" id="radio2" onClick="show_hide('drname', 'symptoms')" />Select doctor's name from the list:</h3>
        <div id="drname" style="display: none;">
		<p>Doctors are Dr.Abhinav Mishra(Allergists/Immunologists), Dr.Shanti yaki(Cardiologists), Dr.Michael dsouza(Endocrinologists),Dr.Pragati Khedekar(Gastroenterologists)</p>
           <!-- <textarea cols="50" rows="10" placeholder="(enter dr. name)"></textarea><br>-->
            <span class="custom-dropdown">

           <select id="doctors" name="doctors">
            <option value="Abhinav Mishra">Abhinav Mishra</option>
            <option value="Shanti yaki">Shanti yaki</option>
            <option value="Michael dsouza">Michael dsouza</option>
            <option value="Pragati Khedekar">Pragati Khedekar</option>
           </select>
       </span>

        </div>
            </div>
            
         
            
        <br />

    
        <h3>DATE:
         <input type="date" id="date" name="date">
        <img src="images/Calendar.png" alt="calender" width="40px" height="40px" style="margin-top:auto">
        </h3><br><br><br>
        <!--<h3>TIME:
            <input type="time" id="time" name="time"></h3><br><br>-->
       <!-- <input type="submit" id="submit" name="submit" class="button doctors_button ml-auto mr-auto" onclick="validate_display()">-->
        <button name="submit" class="footer_contact_button"  onclick="validate_display()"><h3><b>Submit</b></h3></button><br><br>
        <br>

        <div id="output">
        </div>
        </form>
        </div>
        
        
        <script type="text/javascript">
            function show_hide(show, hide)
            {
                document.getElementById(show).style.display="block";
                document.getElementById(hide).style.display="none";
            }

            function validate_display()
            {
                var date1 = document.getElementById("date").value;
                 /* var time1 = document.getElementById("time").value;*/
                if(document.getElementById("radio1").checked)
                {
                     var symptom =document.getElementById("problems").value;
                     if(symptom == "Asthma")
                      {
                       document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>"+date1 ;
                      }

                      else if(symptom == "Eczema")
                        {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }

                         else if(symptom == "Food allergies")
                       {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }

                         else if(symptom == "Insect sting allergies")
                         {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }
                   else if(symptom == "High blood pressure")
                        {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }

                         else if(symptom == "Heart failure")
                         {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }

                         else if(symptom == "Heart attack")
                        {
                        document.getElementById("output").innerHTML = document.getElementById("problems").value +"<br>"+ date1;
                         }
                         else if(symptom == "Diabetes")
                               {
                                document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>"+date1;
                                 }

                                 else if(symptom == "Thyroid problems")
                                 {
                                document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>"+date1;
                                 }
                                 else if(symptom == "Infertility")
                                 {
                                document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>"+date1;
                                 }
                                 else if(symptom == "Calcium and bone disorders")
                                 {
                                document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>"+date1;
                                 }
                                
                                  else if(symptom == "Abdominal pain")
                                   {
                                    document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>" +date1;
                                     }

                                     else if(symptom == "Diarrhea")
                                   {
                                    document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>" +date1 ;
                                     }

                                     else if(symptom == "Jaundice")
                                   {
                                    document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>" +date1;
                                     }

                                     else if(symptom == "Cancers in your digestive organs")
                                   {
                                    document.getElementById("output").innerHTML = document.getElementById("problems").value + "<br>" +date1 ;
                                     }





                                     else
                                     {
                                        document.getElementById("output").innerHTML = alert("enter required details");
                                     }
                }


                else if(document.getElementById("radio2").checked)
                {
                    var drname =document.getElementById("doctors").value;
                     if(drname == "Abhinav Mishra")
                      {
                       document.getElementById("output").innerHTML = document.getElementById("doctors").value + "<br>"+ date1 ;
                      }
                   else if(drname == "Shanti yaki")
                    {
                        document.getElementById("output").innerHTML = document.getElementById("doctors").value + "<br>"+ date1  ;
                         }
                         else if(drname == "Michael dsouza")
                            {
                                document.getElementById("output").innerHTML = document.getElementById("doctors").value +"<br>"+  date1  ;
                                 }
                                  else if(drname == "Pragati Khedekar")
                                   {
                                    document.getElementById("output").innerHTML = document.getElementById("doctors").value+ "<br>"+ date1;
                                     }

                                     else
                                     {
                                        document.getElementById("output").innerHTML = alert("enter required details");
                                     }                                     
                }
         }

        </script>


        <?php
$user_name = "id8984764_root";
$password = "password";
$host_name = "localhost"; 

$conn=mysqli_connect('localhost','id8984764_root','password','id8984764_drwebsite') or die("could not connect to internet");
$selected=mysqli_select_db($conn,"id8984764_drwebsite");
                    if(isset($_POST['search']))
                    {
                    if($_POST['search'] == "symptoms" )
                    {
                    $symptom_name=$_POST['problems'];
                    $date = $_POST['date'];

                    $result=mysqli_query($conn,"select d_name from doctor where d_specialization=(select specialization from  symptoms_match where symptoms='$symptom_name')") or die("Failed aapko kt hai".mysqli_error());
                    $row = mysqli_fetch_array($result);

                    $pname = 'patient name';
					$time = 8;
                    $drname = $row['d_name'];  
					 $count1 = mysqli_query($conn,"SELECT COUNT(a_d_name) AS num FROM predicted_appointed_table WHERE ( a_d_name = '$drname' AND a_date = '$date')");
					 $count2 = mysqli_fetch_assoc($count1);
					 $count3 = $count2['num'];
					 $limit = 5;
					 if($count3<$limit)
					 {
					 
						$xtime = $time+$count3;
						 $insert= mysqli_query($conn,"INSERT INTO predicted_appointed_table(a_symptoms, a_d_name, a_date, a_time, a_p_name) VALUES ('$symptom_name','$drname','$date','$xtime','$pname')");
						 echo '<script language="javascript">';
							echo 'alert("Appointment fixed 1")';
							echo '</script>';
					 }
					 else
					 {
						 echo '<script language="javascript">';
							echo 'alert("Please select other date of appointment 1")';
							echo '</script>';
					 }
                    echo "<div class='home_container'>
            <div class='container'>
                <div class='row'>
                    <div class='col'>
                    <div class='home_content'>

                    <div class='home_title' style='background-color:#32c69a; font-size:20px; align-items-center;height:100px;'> <h4>Specialization: {$row['d_name']}</h4><br><h4>no of appnt day and doctor :{$count3}</h4></div></div>
                    </div>
                </div>
            </div>
        </div><br><br><br><br><br><br><br>";
    }
 else 
 {

    $drname=$_POST['doctors'];
    $date = $_POST['date'];
    $symptom_name="private";

                    $pname = 'patient name 2';
                    $time=8;



					$count1 = mysqli_query($conn,"SELECT COUNT(a_d_name) AS num FROM predicted_appointed_table WHERE ( a_d_name = '$drname' AND a_date = '$date')");
					 $count2 = mysqli_fetch_assoc($count1);
					 $count3 = $count2['num'];
					 $limit = 5;
					 if($count3<$limit)
					 {
					 
						$xtime = $time+$count3;
						 $insert= mysqli_query($conn,"INSERT INTO predicted_appointed_table(a_symptoms, a_d_name, a_date, a_time, a_p_name) VALUES ('$symptom_name','$drname','$date','$xtime','$pname')");
						 echo '<script language="javascript">';
							echo 'alert("Appointment fixed")';
							echo '</script>';
					 }
					 else
					 {
						 echo '<script language="javascript">';
							echo 'alert("Please select other date of appointment")';
							echo '</script>';
					 }
                     

 
  echo "<div class='home_container'>
            <div class='container'>
                <div class='row'>
                    <div class='col'>
                    <div class='home_content'>

                    <div class='home_title' style='background-color:#32c69a; font-size:20px; align-items-center;height:100px;'> <h4>Specialization: {$drname}</h4><br><h4>no of appnt day and doctor :{$count3}</h4></div></div>
                    </div>
                </div>
            </div>
        </div><br><br><br><br><br><br><br>";
 }
 }    
    ?>
        
            </div>
        </div>
		
    <!-- Footer -->

    <footer class="footer">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/footer.jpg" data-speed="0.8"></div>
        <div class="footer_content">
            <div class="container">
                <div class="row">

                    <!-- Footer About -->
                    <div class="col-lg-3 footer_col">
                        <div class="footer_about">
                            <div class="logo">
                                <a href="#">health<span>+</span></a>    
                            </div>
                            <div class="footer_about_text">Lorem ipsum dolor sit amet, lorem maximus consectetur adipiscing elit. Donec malesuada lorem maximus mauris.</div>
                            <div class="footer_social">
                                <ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
                        </div>
                    </div>
                    
                    <!-- Footer Contact -->
                    <div class="col-lg-5 footer_col">
                        <div class="footer_contact">
                            <div class="footer_contact_title">Quick Contact</div>
                            <div class="footer_contact_form_container">
                                <form action="#" class="footer_contact_form" id="footer_contact_form">
                                    <div class="d-flex flex-xl-row flex-column align-items-center justify-content-between">
                                        <input type="text" class="footer_contact_input" placeholder="Name" required="required">
                                        <input type="email" class="footer_contact_input" placeholder="E-mail" required="required">
                                    </div>
                                    <textarea class="footer_contact_input footer_contact_textarea" placeholder="Message" required="required"></textarea>
                                    <button class="footer_contact_button">send message</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Footer Hours -->
                    <div class="col-lg-4 footer_col">
                        <div class="footer_hours">
                            <div class="footer_hours_title">Opening Hours</div>
                            <ul class="hours_list">
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div>Monday – Thursday</div>
                                    <div class="ml-auto">8.00 – 19.00</div>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div>Friday</div>
                                    <div class="ml-auto">8.00 - 18.30</div>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div>Saturday</div>
                                    <div class="ml-auto">9.30 – 17.00</div>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div>Sunday</div>
                                    <div class="ml-auto">9.30 – 15.00</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bar">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer_bar_content d-flex flex-sm-row flex-column align-items-lg-center align-items-start justify-content-start">
                            <nav class="footer_nav">
                                <ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                    <li class="active"><a href="index.html">Home</a></li>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="news.html">News</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                            <div class="footer_links">
                                <ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                                    <li><a href="#">Help Desk</a></li>
                                    <li><a href="#">Emergency Services</a></li>
                                    <li><a href="#">Appointment</a></li>
                                </ul>
                            </div>
                            <div class="footer_phone ml-lg-auto">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>+34 586 778 8892</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about.js"></script>
</body>
</html>